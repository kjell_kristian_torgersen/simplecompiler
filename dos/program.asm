[BITS 16]
[ORG 0x0100]
[SECTION .text]
Start: 
	call main
	mov ax, 04C00H
	int 21H
add:
	pop dx
	pop ax
	pop bx
	add ax,bx
	push ax
	push dx
	ret
sub:
	pop dx
	pop ax
	pop bx
	sub ax,bx
	push ax
	push dx
	ret
newline:
	mov dx, newlinestr
	mov ah,9
	int 21h
	ret
print:
	pop bx
	pop dx
	mov ah,9
	int 21h
	push bx
	ret
putchar:
	pop bx
	pop dx
	mov ah,2
	int 21h
	push bx
	ret
; func println str
println:
	push bp
	mov bp,sp
; 	str print newline
	push word [bp+4] ;str
	call print
	call newline

	mov sp,bp
	pop bp
	ret ;func println str
; func main
main:
	push bp
	mov bp,sp
; 	"Hello, World!$" println 
	push str0
	call println
; 	65 putchar
	push 65
	call putchar
; 	"Good Bye World$" println
	push str1
	call println

	mov sp,bp
	pop bp
	ret ;func main
[SECTION .data]
str0	db	"Hello, World!$"
str1	db	"Good Bye World$"
newlinestr	db	10,13,'$'
