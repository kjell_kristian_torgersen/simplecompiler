[BITS 16]
[ORG 0x0100]
[SECTION .text]
Start: 
	call main
	mov ax, 04C00H
	int 21H
add:
	pop dx
	pop ax
	pop bx
	add ax,bx
	push ax
	push dx
	ret
sub:
	pop dx
	pop ax
	pop bx
	sub ax,bx
	push ax
	push dx
	ret
newline:
	mov dx, newlinestr
	mov ah,9
	int 21h
	ret
print:
	pop bx
	pop dx
	mov ah,9
	int 21h
	push bx
	ret
putchar:
	pop bx
	pop dx
	mov ah,2
	int 21h
	push bx
	ret