﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCompiler
{
    public class Compiler
    {
        StringBuilder program = new StringBuilder();
        List<string> strings = new List<string>();

        string[] customSplit(string input)
        {
            List<string> output = new List<string>();
            string currentWord = "";
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != ' ' && input[i] != '\t')
                {
                    currentWord += input[i];
                }
                else
                {
                    if (currentWord != "")
                    {
                        output.Add((string)currentWord.Clone());
                        currentWord = "";
                    }
                }

                if (input[i] == '"')
                {
                    //currentWord += input[i];
                    i++;
                    while (input[i] != '"')
                    {
                        currentWord += input[i];
                        i++;
                    }
                    currentWord += input[i];
                }
            }
            if(currentWord != "") output.Add(currentWord);
            return output.ToArray();
        }
        int parseFunction(string[] code, int idx)
        {
            int i;
            List<string> args = new List<string>();
            List<string> locals = new List<string>();
            //List<string> expressions = new List<string>();
            StringBuilder expressions = new StringBuilder();
            string[] words;
            string fname = "", funcdef = "";
            for (;;)
            {
                words = customSplit(code[idx]);
                if (words[0] == "func")
                {
                    fname = words[1];

                    for (i = 2; i < words.Length; i++)
                    {
                        args.Add(words[i]);
                    }
                    funcdef = code[idx];


                }
                else if (words[0] == "var")
                {
                    for (i = 1; i < words.Length; i++)
                    {
                        locals.Add(words[i]);
                    }
                }
                else if (words[0] == "endfunc")
                {
                    break;
                }
                else
                {
                    expressions.Append(parseExpression(args, locals, code, idx));
                }
                idx++;
            };

            program.AppendLine("; " + funcdef);
            program.AppendLine(fname + ":");
            program.AppendLine("\tpush bp");
            program.AppendLine("\tmov bp,sp");
            if (locals.Count != 0) program.AppendLine("\tsub sp, " + 2 * locals.Count);
            program.AppendLine(expressions.ToString());

            program.AppendLine("\tmov sp,bp");
            program.AppendLine("\tpop bp");
            program.AppendLine("\tret ;" + funcdef);
            return idx;

        }

        private string parseExpression(List<string> args, List<string> locals, string[] code, int idx)
        {
            string result = "";
            string[] words = customSplit(code[idx]);

            result += "; " + code[idx] + Environment.NewLine;
            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].All(char.IsDigit))
                {
                    result += "\tpush " + words[i] + Environment.NewLine;
                }
                else if (args.Contains(words[i]))
                {
                    int index = args.IndexOf(words[i]);
                    //result += ";\t\"push arg " + words[i] + "\"" + Environment.NewLine;
                    result += "\tpush word [bp+" + (2 + index) * 2 + "] ;" + words[i] + Environment.NewLine;
                    //result += "\tpush bx" + Environment.NewLine;

                }
                else if (locals.Contains(words[i]))
                {
                    int index = locals.IndexOf(words[i]);
                    result += "\tpush word [bp-" + (1 + index) * 2 + "] ; " + words[i] + Environment.NewLine;
                    //result += "\tpush bx" + Environment.NewLine;
                    //result += ";\"push local " + words[i] + "\"" + Environment.NewLine;
                }
                else if (words[i][0] == '#')
                {
                    int index = locals.IndexOf(words[i].Substring(1));
                    result += "\tpush word [bp-" + (1 + index) * 2 + "] ; " + words[i] + Environment.NewLine;
                    //result += "\tpush bx" + Environment.NewLine;
                    //result += ";\t\"pop " + words[i] + "\"" + Environment.NewLine;
                }
                else if (words[i][0] == '"') // a string
                {
                    strings.Add(words[i]);
                    result += "\tpush str" + (strings.Count - 1) + Environment.NewLine;
                    //result += "\tpush bx" + Environment.NewLine;
                }
                else
                {
                    result += "\tcall " + words[i] + Environment.NewLine;
                }
            }

            return result;
        }

        public void parseProgram(string[] code, int idx)
        {
            List<string> globals = new List<string>();
            /*program.AppendLine("[BITS 16]");
            program.AppendLine("[ORG 0x0100]");
            program.AppendLine("[SECTION .text]");
            program.AppendLine("Start: ");
            program.AppendLine("\tcall main");
            program.AppendLine("\tmov ax, 04C00H");
            program.AppendLine("\tint 21H");
            program.AppendLine("add:");
            program.AppendLine("\tpop dx");
            program.AppendLine("\tpop ax");
            program.AppendLine("\tpop bx");
            program.AppendLine("\tadd ax,bx");
            program.AppendLine("\tpush ax");
            program.AppendLine("\tpush dx");
            program.AppendLine("\tret");
            program.AppendLine("print:");
            program.AppendLine("\tpop bx");
            program.AppendLine("\tpop dx");
            program.AppendLine("\tmov ah,9");
            program.AppendLine("\tint 21h");
            program.AppendLine("\tpush bx");
            program.AppendLine("\tret");*/
            program.AppendLine(File.ReadAllText(@"C:\Users\Kjell\Documents\Visual Studio 2015\Projects\SimpleCompiler\dos\lib.asm"));

            do
            {
                string[] words = customSplit(code[idx]);
                if (words[0] == "func")
                {
                    idx = parseFunction(code, idx);
                }
                else if (words[0] == "var")
                {
                    for (int i = 1; i < words.Length; i++)
                    {
                        globals.Add(words[i]);
                    }
                }
                idx++;
            } while (idx < code.Length);
            program.AppendLine("[SECTION .data]");
            for (int i = 0; i < globals.Count; i++)
            {
                program.AppendLine(globals[i] + "\tDW\t0");
            }

            for (int i = 0; i < strings.Count; i++)
            {
                program.AppendLine("str" + i + "\tdb\t" + strings[i]);
            }

            program.AppendLine(File.ReadAllText(@"C:\Users\Kjell\Documents\Visual Studio 2015\Projects\SimpleCompiler\dos\globals.asm"));
            File.WriteAllText(@"C:\Users\Kjell\Documents\Visual Studio 2015\Projects\SimpleCompiler\dos\program.asm", program.ToString());
            System.Diagnostics.Process.Start(@"C:\Users\Kjell\Documents\Visual Studio 2015\Projects\SimpleCompiler\dos\compile.bat");
        }
    }
}
